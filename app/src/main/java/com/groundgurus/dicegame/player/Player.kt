package com.groundgurus.dicegame.player

data class Player(var name: String, var score: Int = 0)