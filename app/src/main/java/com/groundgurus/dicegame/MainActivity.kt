package com.groundgurus.dicegame

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.groundgurus.dicegame.player.Player
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var currentPlayer: Player
    lateinit var player1: Player
    lateinit var player2: Player
    lateinit var rollDiceButton: Button
    lateinit var diceTextView: TextView
    lateinit var currentPlayerTextView: TextView
    lateinit var player1ScoreTextView: TextView
    lateinit var player2ScoreTextView: TextView
    val winningScore: Int = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialize()

        rollDiceButton.setOnClickListener {
            // set a random value
            val randVal = Random().nextInt(6) + 1
            diceTextView.text = randVal.toString()

            // increment the score of the current player
            increaseScore(currentPlayer, randVal)

            // check the winner
            val hasWinner = checkWinner()

            if (!hasWinner) {
                // update the scoreboard for current player
                updateScoreboard(currentPlayer)

                // switch to the next player
                switchPlayer()
            } else {
                Toast.makeText(this, "${currentPlayer.name} wins with the score of ${currentPlayer.score}!", Toast.LENGTH_LONG).show()
                reset()
            }
        }
    }

    private fun initialize() {
        player1 = Player("Player 1")
        player2 = Player("Player 2")
        currentPlayer = player1

        initializeViews()
    }

    private fun initializeViews() {
        rollDiceButton = findViewById(R.id.rollDiceButton)
        diceTextView = findViewById(R.id.diceTextView)
        currentPlayerTextView = findViewById(R.id.currentPlayerNameTextView)
        player1ScoreTextView = findViewById(R.id.player1ScoreTextView)
        player2ScoreTextView = findViewById(R.id.player2ScoreTextView)
    }

    private fun switchPlayer() {
        currentPlayer = if (currentPlayer == player1) player2 else player1
        currentPlayerTextView.text = currentPlayer.name
    }

    private fun increaseScore(player: Player, additionalScore: Int) {
        player.score = player.score + additionalScore
    }

    private fun updateScoreboard(player: Player) {
        if (player == player1) {
            player1ScoreTextView.text = player.score.toString()
        } else {
            player2ScoreTextView.text = player.score.toString()
        }
    }

    private fun checkWinner(): Boolean {
        return currentPlayer.score >= winningScore
    }

    private fun reset() {
        player1ScoreTextView.text = getString(R.string.zeroValue)
        player2ScoreTextView.text = getString(R.string.zeroValue)
        player1.score = 0
        player2.score = 0
    }
}
